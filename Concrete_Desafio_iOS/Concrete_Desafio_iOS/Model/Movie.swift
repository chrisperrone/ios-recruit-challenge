//
//  Movie.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 07/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieResponse: Mappable {
    var results: [Movie]?

    required init?(map: Map) {
        //
    }

    func mapping(map: Map) {
        results <- map["results"]
    }
}
class Movie: Mappable {

    var id: Int?
    var title: String?
    var overview: String?
    var voteCount: Int?
    var releaseDate: String?
    var voteAverage: Double?
    var genreIDs: [Int]?
    var posterPath: String?

    required init?(map: Map) {
        //
    }

    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        overview <- map["overview"]
        voteCount <- map["vote_count"]
        releaseDate <- map["release_date"]
        voteAverage <- map["vote_average"]
        genreIDs <- map["genre_ids"]
        posterPath <- map["backdrop_path"]
    }
}
