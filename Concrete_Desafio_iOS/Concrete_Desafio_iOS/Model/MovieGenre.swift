//
//  MovieGenre.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 09/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import Foundation
import ObjectMapper

class GenreResponse: Mappable {
    var genres: [MovieGenre]?

    required init?(map: Map) {
        //
    }

    func mapping(map: Map) {
        genres <- map["genres"]
    }
}

class MovieGenre: Mappable {

    var id: Int?
    var name: String?

    required init?(map: Map) {
        //
    }

    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
