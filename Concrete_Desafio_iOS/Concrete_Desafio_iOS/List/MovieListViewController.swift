//
//  MovieListViewController.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 08/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import UIKit
import Kingfisher

class MovieListViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    
    let movieListPresenter = MovieListPresenter()
    var popularMovies = [Movie]()
    var selectedMovie: Movie?
    var genres: GenreResponse?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.movieListPresenter.setDelegate(view: self)
        self.movieListPresenter.getMovieGenres()
        self.movieListPresenter.getPopularMovies()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails", let detailsViewController = segue.destination as? MovieDetailsViewController {
            detailsViewController.selectedMovie = self.selectedMovie
            detailsViewController.genreResponse = self.genres
        }
    }


}

extension MovieListViewController: MovieListView {
    func showErrorAlert(message: String) {
        let errorController = UIAlertController(title: "Erro!", message: message, preferredStyle: .alert)
        let errorAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        errorController.addAction(errorAction)
        self.present(errorController, animated: true, completion: nil)
    }

    func setMovieGenres(genres: GenreResponse?) {
        self.genres = genres
    }


    func setPopularMoviesArray(popularMovies: [Movie]) {
        self.popularMovies = popularMovies
        collectionView.reloadData()
    }

}

extension MovieListViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.popularMovies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieListCollectionViewCell.identifier, for: indexPath) as? MovieListCollectionViewCell else { return UICollectionViewCell() }
        let movie = popularMovies[indexPath.row]
        cell.setup(movie: movie)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedMovie = popularMovies[indexPath.row]
        self.selectedMovie = selectedMovie
        performSegue(withIdentifier: "showDetails", sender: nil)
    }

}
