//
//  MovieListService.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 08/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class MovieListService {
    let apiKey = "02aa68fc4db4bd61d4af06e344e8de13"

    func getPopularMovies(completion: @escaping (Bool, MovieResponse?) -> Void) {
        let url = "https://api.themoviedb.org/3/movie/popular?api_key=\(apiKey)"
        Alamofire.request(url).responseObject { (response: DataResponse<MovieResponse>) in
            let success = response.result.isSuccess
            if success, let value = response.result.value {
                completion(success, value)
            } else {
                completion(success, nil)
            }
        }
    }

    func getMovieGenres(completion: @escaping (Bool, GenreResponse?) -> Void) {
        let url = "https://api.themoviedb.org/3/genre/movie/list?api_key=\(apiKey)"
        Alamofire.request(url).responseObject { (response: DataResponse<GenreResponse>) in
            let success = response.result.isSuccess
            if success, let value = response.result.value {
                completion(success, value)
            } else {
                completion(success, nil)
            }
        }
    }
}
