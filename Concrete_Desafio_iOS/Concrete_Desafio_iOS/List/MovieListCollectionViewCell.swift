//
//  MovieListCollectionViewCell.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 08/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import UIKit

class MovieListCollectionViewCell: UICollectionViewCell {

    static let identifier = "movieListCollectionCell"
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!

    func setup(movie: Movie) {
        if let urlPath = movie.posterPath, let url = URL(string: "https://image.tmdb.org/t/p/w500/\(urlPath)") {
            self.posterImageView.kf.indicatorType = .activity
            self.posterImageView.kf.setImage(with: url)
        }
        self.titleLabel.text = movie.title
        if let voteAverage = movie.voteAverage {
            self.voteAverageLabel.text = "\(voteAverage.rounded())"
        }
    }
}
