//
//  MovieListPresenter.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 08/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import Foundation

protocol MovieListView: NSObjectProtocol {
    func setPopularMoviesArray(popularMovies: [Movie])
    func setMovieGenres(genres: GenreResponse?)
    func showErrorAlert(message: String)
}

class MovieListPresenter {

    let movieListService = MovieListService()
    weak var movieListView: MovieListView?

    let message = "Falha na requisição!"
    var genresArray = [(id: Int, name: String)]()

    func setDelegate(view: MovieListView) {
        self.movieListView = view
    }

    func getPopularMovies() {
        self.movieListService.getPopularMovies { (success, response) in
            if success {
                if let movies = response?.results {
                    self.movieListView?.setPopularMoviesArray(popularMovies: movies)
                }
            } else {
                self.movieListView?.showErrorAlert(message: self.message)
            }
        }
    }

    func getMovieGenres() {
        self.movieListService.getMovieGenres { (success, value) in
            if success {
                self.movieListView?.setMovieGenres(genres: value)
            } else {
                self.movieListView?.showErrorAlert(message: self.message)
            }
        }
    }
}
