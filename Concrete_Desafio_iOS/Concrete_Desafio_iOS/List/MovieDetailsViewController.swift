//
//  MovieDetailsViewController.swift
//  Concrete_Desafio_iOS
//
//  Created by Christian Perrone on 08/07/18.
//  Copyright © 2018 Christian Perrone. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!

    var selectedMovie: Movie?
    var genreResponse: GenreResponse?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setup(selectedMovie: selectedMovie)
    }

    private func setup(selectedMovie: Movie?) {
        if let movie = selectedMovie {
            self.posterImageView.backgroundColor = .orange
            if let urlPath = movie.posterPath, let url = URL(string: "https://image.tmdb.org/t/p/w500/\(urlPath)") {
                self.posterImageView.kf.indicatorType = .activity
                self.posterImageView.kf.setImage(with: url)
            }
            self.titleLabel.text = movie.title
            self.overviewTextView.text = movie.overview
            guard let voteCount = movie.voteCount else { return self.voteCountLabel.text = "000" }
            self.voteCountLabel.text = "Total votes: \(voteCount)"
            guard let voteAverage = movie.voteAverage else { return self.voteAverageLabel.text = "000" }
            self.voteAverageLabel.text = "Note average: \(voteAverage.rounded())"
            self.releaseDateLabel.text = movie.releaseDate

            if let genreIDs = movie.genreIDs, let genres = genreResponse?.genres {
                var genreText = "|"
                for genreID in genreIDs {
                    for genre in genres where genreID == genre.id {
                        genreText = genreText + (genre.name ?? "") + "|"
                    }
                }
                self.genreLabel.text = genreText
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
